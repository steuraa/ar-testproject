﻿using System;
using System.Collections.Generic;
using UnityEngine.XR.ARSubsystems;
using Unity.Collections;

namespace UnityEngine.XR.ARFoundation
{
  /// <summary>
  /// Generates a mesh for an <see cref="ARPlane"/>.
  /// </summary>
  /// <remarks>
  /// If this <c>GameObject</c> has a <c>MeshFilter</c> and/or <c>MeshCollider</c>,
  /// this component will generate a mesh from the underlying <c>BoundedPlane</c>.
  ///
  /// It will also update a <c>LineRenderer</c> with the boundary points, if present.
  /// </remarks>
  [RequireComponent(typeof(ARPlane))]
  public sealed class ARCustomPlaneMeshVisualizer : MonoBehaviour
  {
    /// <summary>
    /// Get the <c>Mesh</c> that this visualizer creates and manages.
    /// </summary>
    public Mesh mesh { get; private set; }
    private Boolean visiblePlane = false;

    void OnBoundaryChanged(ARPlaneBoundaryChangedEventArgs eventArgs)
    {
      var boundary = m_Plane.boundary;
      // NativeArray<Vector2> square = CreateSquareBoundaries();
      if (!ARPlaneMeshGenerators.GenerateMesh(mesh, new Pose(transform.localPosition, transform.localRotation), boundary))
        return;

      var lineRenderer = GetComponent<LineRenderer>();
      if (lineRenderer != null)
      {
        lineRenderer.positionCount = boundary.Length;
        for (int i = 0; i < boundary.Length; ++i)
        {
          var point2 = boundary[i];
          lineRenderer.SetPosition(i, new Vector3(point2.x, 0, point2.y));
        }
      }

      var meshFilter = GetComponent<MeshFilter>();
      if (meshFilter != null)
        meshFilter.sharedMesh = mesh;

      var meshCollider = GetComponent<MeshCollider>();
      if (meshCollider != null)
        meshCollider.sharedMesh = mesh;
    }

    NativeArray<Vector2> CreateSquareBoundaries()
    {
      Vector2[] boundariesVector = new Vector2[4];
      Vector2 center = m_Plane.centerInPlaneSpace;
      Vector2 extents = m_Plane.extents;
      boundariesVector[0] = new Vector2(center.x + extents.x, center.y - extents.y);
      boundariesVector[1] = new Vector2(center.x + extents.x, center.y + extents.y);
      boundariesVector[2] = new Vector2(center.x - extents.x, center.y + extents.y);
      boundariesVector[3] = new Vector2(center.x - extents.x, center.y - extents.y);
      NativeArray<Vector2> boundaries = new NativeArray<Vector2>(boundariesVector, Allocator.Temp);

      return boundaries;
    }
    void DisableComponents()
    {
      enabled = false;

      var meshCollider = GetComponent<MeshCollider>();
      if (meshCollider != null)
        meshCollider.enabled = false;

      UpdateVisibility();
    }

    public void MakePlaneVisible(bool visible)
    {
      visiblePlane = visible;
    }
    void SetVisible(bool visible)
    {
      var meshRenderer = GetComponent<MeshRenderer>();
      if (meshRenderer != null)
        meshRenderer.enabled = visible;

      var lineRenderer = GetComponent<LineRenderer>();
      if (lineRenderer != null)
        lineRenderer.enabled = visible;
    }

    void UpdateVisibility()
    {
      var visible = enabled &&
          (m_Plane.trackingState != TrackingState.None) &&
          (ARSession.state > ARSessionState.Ready) &&
          (m_Plane.subsumedBy == null) && visiblePlane;

      SetVisible(visible);
    }

    void Awake()
    {
      mesh = new Mesh();
      m_Plane = GetComponent<ARPlane>();
    }

    void OnEnable()
    {
      m_Plane.boundaryChanged += OnBoundaryChanged;
      UpdateVisibility();
      OnBoundaryChanged(default(ARPlaneBoundaryChangedEventArgs));
    }

    void OnDisable()
    {
      m_Plane.boundaryChanged -= OnBoundaryChanged;
      UpdateVisibility();
    }

    void Update()
    {
      if (transform.hasChanged)
      {
        var lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer != null)
        {
          if (!m_InitialLineWidthMultiplier.HasValue)
            m_InitialLineWidthMultiplier = lineRenderer.widthMultiplier;

          lineRenderer.widthMultiplier = m_InitialLineWidthMultiplier.Value * transform.lossyScale.x;
        }
        else
        {
          m_InitialLineWidthMultiplier = null;
        }

        transform.hasChanged = false;
      }

      if (m_Plane.subsumedBy != null)
      {
        DisableComponents();
      }
      else
      {
        UpdateVisibility();
      }
    }

    float? m_InitialLineWidthMultiplier;

    ARPlane m_Plane;
  }
}
