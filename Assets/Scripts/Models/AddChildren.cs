﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Threading.Tasks;

public class AddChildren : MonoBehaviour
{
  private List<GameObject> children;
  // Start is called before the first frame update
  [SerializeField]
  private string childrenAdress;
  private GameObject modelInstance;
  void Awake()
  {
    children = this.GetComponentsInChildren<Transform>()
    .Where(transform => transform.CompareTag("ModelParent")).Select(transform => transform.gameObject).ToList();
  }

  // Update is called once per frame
  void Update()
  {

  }

  public async void AddChildModels()
  {
    if (modelInstance == null)
    {
      AsyncOperationHandle<GameObject> goHandle = Addressables.LoadAssetAsync<GameObject>(childrenAdress);
      await goHandle.Task;
      if (goHandle.Task.Status == TaskStatus.RanToCompletion)
      {
        modelInstance = goHandle.Task.Result;
      }
    }
    foreach (var child in children)
    {
      var instance = Instantiate(modelInstance, new Vector3(), new Quaternion());
      instance.transform.SetParent(child.transform, false);
    }

  }
  private void LoadNewModel(AsyncOperationHandle<GameObject> obj)
  {
    switch (obj.Status)
    {
      case AsyncOperationStatus.Succeeded:
        GameObject modelPrefab = obj.Result;
        modelInstance = Instantiate(modelPrefab, new Vector3(), new Quaternion());
        break;
      case AsyncOperationStatus.Failed:
        Debug.LogError("Model load failed. ");
        break;
      default:
        // case AsyncOperationStatus.None:
        break;
    }
  }
}
