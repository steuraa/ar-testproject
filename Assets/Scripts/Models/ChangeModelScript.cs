﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.AR;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ChangeModelScript : MonoBehaviour
{
  [SerializeField]
  private ModelManager modelManager;
  void Awake() { }

  public void ChangePrefabTo(string prefabName)
  {
    Addressables.LoadAssetAsync<GameObject>(prefabName).Completed += ModelLoaded;
  }
  private void ModelLoaded(AsyncOperationHandle<GameObject> obj)
  {
    switch (obj.Status)
    {
      case AsyncOperationStatus.Succeeded:
        GameObject loadedObject = obj.Result;
        // ModelManager.Instance.ChangeModelPrefab(loadedObject);
        break;
      case AsyncOperationStatus.Failed:
        Debug.LogError("Model load failed. ");
        break;
      default:
        // case AsyncOperationStatus.None:
        break;
    }
  }
}
