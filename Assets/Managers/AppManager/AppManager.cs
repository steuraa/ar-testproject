﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : Manager<AppManager>
{
    public enum AppState
    {
        AR_MENU,
        RUNNING,
        PRODUCTS_MENU,
        AUTH_MENU,
        LOGIN_MENU,
        SIGNUP_MENU,
        START_MENU,
        WELCOME_MENU
    }
    // load other persistent systems
    public GameObject[] SystemPrefabs;    // Prefabs we can load through the Editor
    public Events.EventAppState OnAppStateChanged;

    private string _currentSceneName = string.Empty;
    private List<GameObject> _instancedSystemPrefabs;    // Instantiated Prefabs
    private List<AsyncOperation> _loadOperations;
    private AppState _currentAppState = AppState.START_MENU;

    public AppState CurrentAppState
    {
        get => _currentAppState;
        private set => _currentAppState = value;
    }

    private void Start()
    {
        Debug.Log($"instancedSystemsPrefabs:: {_instancedSystemPrefabs}");
        _instancedSystemPrefabs = new List<GameObject>();
        _loadOperations = new List<AsyncOperation>();

        InstantiateSystemPrefabs();
    }

    public void LoadScene(string sceneName)
    {
        // Replace current scene with a new scene, saving a reference to the async operation
        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        if (ao == null)
        {
            Debug.LogErrorFormat("[App Manager] Unable to load scene {0}", sceneName);
            return;
        }
        // Add a listener to the operation completed event
        ao.completed += OnLoadOperationComplete;
        // Add to our List of load operations
        _loadOperations.Add(ao);
        // Update current scene name
        _currentSceneName = sceneName;
    }

    private void OnLoadOperationComplete(AsyncOperation ao)
    {
        if (_loadOperations.Contains(ao))
        {
            _loadOperations.Remove(ao);

            if (_loadOperations.Count == 0)
            {
                UpdateState(AppState.RUNNING);
            }
        }
        Debug.Log("Load Complete.");
    }

    private void UpdateState(AppState state)
    {
        AppState previousAppState = _currentAppState;
        _currentAppState = state;

        switch (_currentAppState)
        {
            case AppState.RUNNING:
                Time.timeScale = 1.0f;
                break;
            case AppState.PRODUCTS_MENU:
                Time.timeScale = 0.0f;
                break;
            default:
                break;
        }
        // dispatch messages
        OnAppStateChanged.Invoke(_currentAppState, previousAppState);
    }

    private void InstantiateSystemPrefabs()
    {
        GameObject prefabInstance;

        for (int i = 0; i < SystemPrefabs.Length; ++i)
        {
            prefabInstance = Instantiate(SystemPrefabs[i]);
            _instancedSystemPrefabs.Add(prefabInstance);
        }
    }

    protected void OnDestroy()
    {
        if (_instancedSystemPrefabs == null)
        {
            return;
        }
        for (int i = 0; i < _instancedSystemPrefabs.Count; ++i)
        {
            Destroy(_instancedSystemPrefabs[i]);
        }
        _instancedSystemPrefabs.Clear();
    }

    public void GoToMenu(string menu)
    {
        switch (menu)
        {
            case "auth":
                UpdateState(AppState.AUTH_MENU);
                break;
            case "login":
                UpdateState(AppState.LOGIN_MENU);
                break;
            case "signup":
                UpdateState(AppState.SIGNUP_MENU);
                break;
            case "welcome":
                UpdateState(AppState.WELCOME_MENU);
                break;
            case "products":
                UpdateState(AppState.PRODUCTS_MENU);
                break;
            case "arMenu":
                UpdateState(AppState.AR_MENU);
                break;
            default:
                break;
        }
    }

    // public void StartGame()
    // {
    //     LoadScene("Main");
    // }

    // public void TogglePause()
    // {
    //     UpdateState(_currentAppState == AppState.RUNNING ? AppState.PAUSED : AppState.RUNNING);
    // }

    // public void RestartGame()
    // {
    //     UpdateState(AppState.PRELOAD);
    // }

    public void QuitGame()
    {
        // implement features for quitting (autosave, etc)
        Application.Quit();
    }
}
