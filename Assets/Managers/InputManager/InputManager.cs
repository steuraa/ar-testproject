﻿using UnityEngine;

public class InputManager : Manager<InputManager>
{
    void Update()
    {
        if (AppManager.Instance.CurrentAppState == AppManager.AppState.START_MENU) {
            if (Input.touchCount > 0 || Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(1)) {
                AppManager.Instance.GoToMenu("products");
            }
        }
    }
}
