﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARMenu : MonoBehaviour
{

  public GameObject panel;
  private Animator panelAnimator;
  public GameObject button;
  private Animator buttonAnimator;
  void Start()
  {
    Debug.Log("ARMenu Start");
    panelAnimator = panel.GetComponent<Animator>();
    if (panelAnimator != null)
    {
      Debug.Log("panelAnimator init");
      if (!panelAnimator.isInitialized)
      {
        panelAnimator.Rebind();
      }
    }
    buttonAnimator = button.GetComponent<Animator>();
    if (buttonAnimator != null)
    {
      if (!buttonAnimator.isInitialized)
      {
        buttonAnimator.Rebind();
      }
    }
  }

  // Update is called once per frame
  public void OpenPanel()
  {
    if (panel != null)
    {
      // Animator panelAnimator = panel.GetComponent<Animator>();
      // if (panelAnimator != null)
      // {
      // if (!panelAnimator.isInitialized)
      // {

      // }
      // panelAnimator.Play("Base Layer.PanelFadeIn");
      panelAnimator.SetBool("open", true);
      // button.SetActive(false);
      // }
      // Animator buttonAnimator = button.GetComponent<Animator>();
      // if(buttonAnimator != null) {
      // buttonAnimator.Play("Base Layer.ButtonFadeOut");
      buttonAnimator.SetBool("open", false);
      // }
    }
  }

  public void HidePanel()
  {
    if (panel != null)
    {
      Animator panelAnimator = panel.GetComponent<Animator>();
      // if (panelAnimator != null)
      // {
      panelAnimator.SetBool("open", false);
      // panelAnimator.Play("Base Layer.PanelFadeOut");
      // }
      // Animator buttonAnimator = button.GetComponent<Animator>();
      // if (buttonAnimator != null)
      // {
      buttonAnimator.SetBool("open", true);
      // buttonAnimator.Play("Base Layer.ButtonFadeIn");
      // }
      // button.SetActive(true);
    }
  }
}
