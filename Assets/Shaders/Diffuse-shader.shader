﻿Shader "Custom/Diffuse-shader"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Scale ("Texture Scale", Float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Lambert

        sampler2D _MainTex;
        fixed4 _Color;
        float _Scale;

        struct Input
        {
            // float2 uv_MainTex;
            float3 worldNormal;
            float3 worldPos;
        };

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

       void surf (Input IN, inout SurfaceOutput o) {
        float2 UV;
        fixed4 c;

        if(abs(IN.worldNormal.x)>0.5) 
        {
            UV = IN.worldPos.yz; // side
            c = tex2D(_MainTex, UV* _Scale); // use WALLSIDE texture
        } 
        else if(abs(IN.worldNormal.z)>0.5) 
        {
            UV = IN.worldPos.xy; // front
            c = tex2D(_MainTex, UV* _Scale); // use WALL texture
        } 
        else 
        {
            UV = IN.worldPos.xz; // top
            c = tex2D(_MainTex, UV* _Scale); // use FLR texture
        }

        o.Albedo = c.rgb * _Color;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
