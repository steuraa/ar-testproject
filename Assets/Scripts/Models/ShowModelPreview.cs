﻿using UnityEngine.XR.Interaction.Toolkit.AR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(ARRaycastManager), typeof(ARPlaneManager))]
public class ShowModelPreview : MonoBehaviour
{
  [SerializeField]
  static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
  private ARRaycastManager aRRaycastManager;
  private ARPlaneManager aRPlaneManager;

  void Awake()
  {
    aRRaycastManager = GetComponent<ARRaycastManager>();
    aRPlaneManager = GetComponent<ARPlaneManager>();
  }

  void OnEnable()
  {
    aRPlaneManager.enabled = true;
  }

  void OnDisable()
  {
    aRPlaneManager.enabled = false;
  }

  public (bool, Vector3) TestPlaneHit()
  {
    (bool, Vector3) returnValue = (false, new Vector3());
    if (aRRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), s_Hits, TrackableType.Planes))
    // if (aRRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), s_Hits, TrackableType.PlaneWithinInfinity) 
    // || aRRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), s_Hits, TrackableType.PlaneWithinInfinity)
    // || aRRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), s_Hits, TrackableType.PlaneEstimated))
    {
      var hit = s_Hits[0];
      var plane = aRPlaneManager.GetPlane(hit.trackableId);
      returnValue = (true, hit.pose.position);
      // if (plane.isActiveAndEnabled && plane.GetComponent<MeshRenderer>().isVisible)
      // {
      //   returnValue = (true, hit.pose.position);
      // }
    }
    return returnValue;
  }
}
