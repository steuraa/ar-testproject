﻿using UnityEngine;
using UnityEngine.Events;

public class Events
{
    [System.Serializable] public class EventFadeComplete : UnityEvent<bool> { }

    [System.Serializable] public class EventAppState : UnityEvent<AppManager.AppState, AppManager.AppState> { }
    [System.Serializable] public class EventModelStateChanged : UnityEvent<ModelManager.ModelState, ModelManager.ModelState> { }
    [System.Serializable] public class EventModelPlaced : UnityEvent<bool> { }
}
