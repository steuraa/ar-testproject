﻿using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ARCreateSinglePlane : MonoBehaviour
{
  [SerializeField]
  private GameObject planePrefab;
  [SerializeField]
  private ARSession aRSession;
  private GameObject placedPlane;
  [SerializeField]
  public ARPlane firstPlacedPlane;
  private ARPlaneManager aRPlaneManager;
  void Awake()
  {
    aRPlaneManager = GetComponent<ARPlaneManager>();
    aRPlaneManager.planesChanged += OnPlanesChanged;
  }

  void OnEnable()
  {
    aRPlaneManager.planesChanged += OnPlanesChanged;
    if (firstPlacedPlane != null)
    {
      firstPlacedPlane.boundaryChanged += OnBoundaryChanged;
    }
  }
  void OnDisable()
  {
    aRPlaneManager.planesChanged -= OnPlanesChanged;
    if (firstPlacedPlane != null)
    {
      firstPlacedPlane.boundaryChanged -= OnBoundaryChanged;
    }
  }

  void OnPlaneAddedSingle(ARPlanesChangedEventArgs eventArgs)
  {
    if (eventArgs.added != null && placedPlane == null)
    {
      placedPlane = Instantiate(planePrefab, eventArgs.added[0].transform.position, Quaternion.identity);
      aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
    }

  }
  void OnPlanesChanged(ARPlanesChangedEventArgs eventArgs)
  {
    firstPlacedPlane = eventArgs.added?[0];
    firstPlacedPlane.GetComponent<MeshRenderer>().enabled = true;
    firstPlacedPlane.gameObject.SetActive(true);
    aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
  }
  // void OnPlanesChanged(ARPlanesChangedEventArgs eventArgs)
  // {
  //   eventArgs.added?.ForEach(plane =>
  //   {
  //     if (firstPlacedPlane && plane.trackableId != firstPlacedPlane.trackableId)
  //     {
  //       plane.gameObject.SetActive(false);
  //     }
  //     if (!firstPlacedPlane && plane.size.x * plane.size.y >= 2f)
  //     {
  //       firstPlacedPlane = plane;
  //       firstPlacedPlane.GetComponent<MeshRenderer>().enabled = true;
  //       // firstPlacedPlane.boundaryChanged += OnBoundaryChanged;
  //     }
  //   });
  //   eventArgs.updated?.ForEach(plane =>
  //   {
  //     if (firstPlacedPlane && plane.trackableId != firstPlacedPlane.trackableId)
  //     {
  //       plane.gameObject.SetActive(false);
  //     }
  //     if (!firstPlacedPlane && plane.size.x * plane.size.y >= 2f)
  //     {
  //       firstPlacedPlane = plane;
  //       firstPlacedPlane.GetComponent<MeshRenderer>().enabled = true;
  //       // firstPlacedPlane.boundaryChanged += OnBoundaryChanged;
  //     }
  //   });
  // }
  void OnBoundaryChanged(ARPlaneBoundaryChangedEventArgs eventArgs)
  {
    // feathered plane takes care of sizing now
    // Vector2 size = firstPlacedPlane.size;
    // firstPlacedPlane.transform.localScale = new Vector3(size.x, 1, size.y);
  }

  public void ResetSession()
  {
    aRSession.Reset();
  }
}
