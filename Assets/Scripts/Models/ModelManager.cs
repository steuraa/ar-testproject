﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


public class ModelManager : Manager<ModelManager>
{
  public enum ModelState
  {
    Selecting, New, Selected, Passive
  }
  [SerializeField]
  private GameObject m_cylinderPrefab;
  [SerializeField]
  private GameObject modelPrefab;
  private GameObject selectedModel;
  private GameObject modelInstance;
  private GameObject cylinderInstance;
  private List<GameObject> prefabs = new List<GameObject>();
  private List<GameObject> instances = new List<GameObject>();

  [SerializeField]
  private ShowModelPreview showModelPreview;

  private ModelState previousModelState = ModelState.Passive;
  private ModelState currentModelState = ModelState.Passive;

  public Events.EventModelStateChanged OnModelChanged;
  public Events.EventModelStateChanged OnStateChanged;


  void Start()
  {
    cylinderInstance = Instantiate(m_cylinderPrefab, new Vector3(), new Quaternion());
    cylinderInstance.SetActive(false);

  }

  public void Update()
  {
    if (currentModelState == ModelState.New)
    {
      var hit = showModelPreview.TestPlaneHit();
      if (hit.Item1)
      {
        cylinderInstance.transform.position = hit.Item2;
        Vector3 upAxis = Vector3.up;
        modelInstance.transform.rotation = Quaternion.LookRotation(Vector3.Cross(upAxis, Vector3.Cross(upAxis, Camera.main.transform.forward)), upAxis);
        // modelInstance.transform.position = new Vector3(hit.Item2.x, hit.Item2.y + 0.3f, hit.Item2.z);
        modelInstance.transform.position = new Vector3(hit.Item2.x, hit.Item2.y, hit.Item2.z);
      }
    }
  }

  public void SelectNewModel()
  {
    ChangeModelState(ModelState.Selecting);
  }
  public void ChangeModelPrefab(string prefabName)
  {
    Addressables.LoadAssetAsync<GameObject>(prefabName).Completed += LoadNewModel;
  }
  private void LoadNewModel(AsyncOperationHandle<GameObject> obj)
  {
    switch (obj.Status)
    {
      case AsyncOperationStatus.Succeeded:
        GameObject modelPrefab = obj.Result;
        modelInstance = Instantiate(modelPrefab, new Vector3(), new Quaternion());
        showModelPreview.enabled = true;
        ChangeModelState(ModelState.New);
        // OnModelChanged.Invoke(ModelState.New, modelInstance);
        break;
      case AsyncOperationStatus.Failed:
        Debug.LogError("Model load failed. ");
        break;
      default:
        // case AsyncOperationStatus.None:
        break;
    }
  }

  public void PlaceModel()
  {
    cylinderInstance.SetActive(false);
    ChangeModelState(ModelState.Passive);
    instances.Add(modelInstance);
    modelInstance = null;
    showModelPreview.enabled = false;
  }

  private void ChangeModelState(ModelState newState)
  {
    previousModelState = currentModelState;
    currentModelState = newState;
    OnStateChanged.Invoke(previousModelState, currentModelState);
  }
}
