﻿using UnityEngine.XR.Interaction.Toolkit.AR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;
using System.Collections.Generic;
public class ARPlacementInteractableBlocked : ARBaseGestureInteractable
{
  public GameObject placementPrefab;
  [SerializeField]
  private ARObjectPlacementEvent onObjectPlaced;

  private GameObject placementObject;

  private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

  private static GameObject trackablesObject;

  private static TapGestureRecognizer tap;

  protected override bool CanStartManipulationForGesture(TapGesture gesture)
  {
    if (gesture.TargetObject == null)
    {
      return true;
    }
    return false;
  }

  protected override void OnEndManipulation(TapGesture gesture)
  {
    if (gesture.WasCancelled)
    {
      return;
    }
    if (gesture.TargetObject != null)
    {
      return;
    }

    var startPosition = gesture.StartPosition;
    bool isOverUI = startPosition.IsPointOverUIObject();

    if (isOverUI)
    {
      return;
    }

    if (!isOverUI && GestureTransformationUtility.Raycast(gesture.StartPosition, hits, TrackableType.PlaneWithinPolygon))
    {
      var hit = hits[0];
      if (Vector3.Dot(Camera.main.transform.position - hit.pose.position, hit.pose.rotation * Vector3.up) < 0)
      {
        return;
      }
      // if (placementObject == null)
      // {
      if (placementPrefab == null)
      {
        return;
      }
      placementObject = Instantiate(placementPrefab, hit.pose.position, hit.pose.rotation);
      var anchorObject = new GameObject("PlacementAnchor");
      anchorObject.transform.position = hit.pose.position;
      anchorObject.transform.rotation = hit.pose.rotation;
      placementObject.transform.parent = anchorObject.transform;

      if (trackablesObject == null)
      {
        trackablesObject = GameObject.Find("Trackables");
      }

      if (trackablesObject != null)
      {
        anchorObject.transform.parent = trackablesObject.transform;
      }

      onObjectPlaced?.Invoke(this, placementObject);
    }
    // }
  }
}