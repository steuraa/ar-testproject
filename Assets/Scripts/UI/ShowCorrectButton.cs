﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ShowCorrectButton : MonoBehaviour
{
  [SerializeField] private Button placeTableButton;
  [SerializeField] private Button addChildrenButton;

  [SerializeField] private GameObject tablePrefab;
  private GameObject instance;
  [SerializeField] private ARRaycastManager aRRaycastManager;
  static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

  void Awake()
  {
    placeTableButton.onClick.AddListener(AddTable);
    addChildrenButton.onClick.AddListener(AddChildren);
  }

  private void AddTable()
  {
    if (aRRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), s_Hits, TrackableType.Planes))
    {
      var hit = s_Hits[0];
      instance = Instantiate(tablePrefab, hit.pose.position, hit.pose.rotation);
      placeTableButton.gameObject.SetActive(false);
      addChildrenButton.gameObject.SetActive(true);
    }
  }

  private void AddChildren()
  {
    if (instance && instance.GetComponent<AddChildren>() != null)
    {
      instance.GetComponent<AddChildren>().AddChildModels();
    }
  }
}
