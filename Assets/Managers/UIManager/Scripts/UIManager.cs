﻿using UnityEngine;

public class UIManager : Manager<UIManager>
{
    [SerializeField] private StartMenu _startMenu;
    [SerializeField] private ProductsMenu _productsMenu;
    [SerializeField] private ARMenu _arMenu;
    // [SerializeField] private AuthMenu _authMenu;
    // [SerializeField] private LoginMenu _loginMenu;
    // [SerializeField] private SignUpMenu _signUpMenu;
    // [SerializeField] private WelcomeMenu _welcomeMenu;
    [SerializeField] private Camera _dummyCamera;

    public Events.EventFadeComplete OnMainMenuFadeComplete;
    private void Start()
    {
        Debug.Log("Start UIManager");
        // _mainMenu.OnMainMenuFadeComplete.AddListener(HandleMainMenuFadeComplete);
        AppManager.Instance.OnAppStateChanged.AddListener(HandleAppStateChanged);
    }

    public void SetDummyCameraActive(bool active)
    {
        _dummyCamera.gameObject.SetActive(active);
    }

    private void HandleAppStateChanged(AppManager.AppState currentState, AppManager.AppState previousState)
    {
        Debug.Log($"currentState: {currentState}");
        _productsMenu.gameObject.SetActive(currentState == AppManager.AppState.PRODUCTS_MENU);
        _startMenu.gameObject.SetActive(currentState == AppManager.AppState.START_MENU);
        _arMenu.gameObject.SetActive(currentState == AppManager.AppState.AR_MENU);
        // _authMenu.gameObject.SetActive(currentState == AppManager.AppState.AUTH_MENU);
        // _loginMenu.gameObject.SetActive(currentState == AppManager.AppState.LOGIN_MENU);
        // _signUpMenu.gameObject.SetActive(currentState == AppManager.AppState.SIGNUP_MENU);
        // _welcomeMenu.gameObject.SetActive(currentState == AppManager.AppState.WELCOME_MENU);
    }

    private void HandleMainMenuFadeComplete(bool fadeOut)
    {
        OnMainMenuFadeComplete.Invoke(fadeOut);
    }
}
