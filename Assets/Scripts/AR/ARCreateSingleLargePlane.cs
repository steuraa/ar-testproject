﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARCreateSingleLargePlane : MonoBehaviour
{
  [SerializeField]
  private ARSession aRSession;
  private GameObject placedPlane;
  [SerializeField]
  public ARPlane firstPlacedPlane;
  private ARPlaneManager aRPlaneManager;
  void Awake()
  {
    aRPlaneManager = GetComponent<ARPlaneManager>();
    aRPlaneManager.planesChanged += OnPlaneAddedSingle;
  }

  void OnEnable()
  {
    aRPlaneManager.planesChanged += OnPlaneAddedSingle;
  }
  void OnDisable()
  {
    aRPlaneManager.planesChanged -= OnPlaneAddedSingle;
  }

  void OnPlaneAddedSingle(ARPlanesChangedEventArgs eventArgs)
  {
    if (eventArgs.added != null)
    {
    //   placedPlane = Instantiate(planePrefab, eventArgs.added[0].transform.position, Quaternion.identity);
      aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
    }

  }
  void OnPlanesChanged(ARPlanesChangedEventArgs eventArgs)
  {
    firstPlacedPlane = eventArgs.added?[0];
    firstPlacedPlane.GetComponent<MeshRenderer>().enabled = true;
    firstPlacedPlane.gameObject.SetActive(true);
    aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
  }
}
