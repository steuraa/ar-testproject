﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARGuiManager : MonoBehaviour
{

  public GameObject modelsPanel;
  public GameObject optionsPanel;
  public GameObject addNewButton;
  public GameObject placeModelButton;
  [SerializeField]
  private ModelManager modelManager;
  // Start is called before the first frame update
  void Awake()
  {
    //   modelManager.OnModelChanged.AddListener(HideModelsPanel);
    modelManager.OnStateChanged.AddListener(ChangeGui);
  }

  public void ChangeGui(ModelManager.ModelState previousState, ModelManager.ModelState currentState)
  {
    switch (currentState)
    {
      case ModelManager.ModelState.New:
        HideModelsPanel();
        break;
      case ModelManager.ModelState.Passive:
        ShowAddNew();
        break;
      case ModelManager.ModelState.Selecting:
        OpenModelsPanel();
        break;
      default:
        break;
    }

  }
  private void HideModelsPanel()
  {
    if (modelsPanel != null)
    {
      modelsPanel.GetComponent<Animator>().SetBool("open", false);
    //   addNewButton.SetActive(false);
      placeModelButton.SetActive(true);
    }
  }

  private void OpenModelsPanel()
  {
    if (modelsPanel != null)
    {
      modelsPanel.GetComponent<Animator>().SetBool("open", true);
      addNewButton.SetActive(false);
    //   addNewButton.GetComponent<Animator>().SetBool("open", false);
    }
  }

  private void ShowAddNew()
  {
      addNewButton.SetActive(true);
    //   addNewButton.GetComponent<Animator>().SetBool("open", true);
      placeModelButton.SetActive(false);
      optionsPanel.SetActive(false);
  }
}
