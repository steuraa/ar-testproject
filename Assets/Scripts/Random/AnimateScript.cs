﻿using UnityEngine;
using UnityEngine.UI;

public class AnimateScript : MonoBehaviour
{

  public GameObject panel;
  private Animator panelAnimator;
  public GameObject button;
  private Animator buttonAnimator;
  void Start()
  {
    panelAnimator = panel.GetComponent<Animator>();
    if (panelAnimator != null)
    {
      if (!panelAnimator.isInitialized)
      {
        panelAnimator.Rebind();
      }
    }
    buttonAnimator = button.GetComponent<Animator>();
    if (buttonAnimator != null)
    {
      button.GetComponent<Image>().color = Color.green;
      if (!buttonAnimator.isInitialized)
        button.GetComponent<Image>().color = Color.magenta;
      {
        buttonAnimator.Rebind();
      }
    }
    else
    {
      button.GetComponent<Image>().color = Color.red;
    }
  }

  // Update is called once per frame
  public void OpenPanel()
  {
    if (panel != null)
    {
      button.GetComponent<Image>().color = Color.red;
      panelAnimator.SetBool("open", true);
      buttonAnimator.SetBool("open", false);
    }
  }

  public void HidePanel()
  {
    if (panel != null)
    {
      button.GetComponent<Image>().color = Color.magenta;
      panelAnimator.SetBool("open", false);
      buttonAnimator.SetBool("open", true);
    }
  }
}
